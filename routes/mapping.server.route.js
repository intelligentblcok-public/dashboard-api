import express from 'express';
//import controller file
import * as setupController from '../controllers/setup.controller';
import * as commisionBlockchainController from '../controllers/commisionBlockchain.controller';


// get an instance of express router
const router = express.Router();

//Setup BlockChain Route
router.route('/Setup')
      .get(setupController.index);

// Map Auto-commision network details
router.route('/networkConfigured')
      .get(commisionBlockchainController.getContainerList)
      .post(commisionBlockchainController.getNetworkConfig);

router.route('/Container')
      .get(commisionBlockchainController.getContainerList)
      .post(commisionBlockchainController.getContainerLogs);

export default router;