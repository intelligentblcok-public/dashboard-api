
'use strict';
const shim = require('fabric-shim');
const util = require('util');

let Chaincode = class {
  // Instansiate the chaincode
  async Init(stub) {
    console.info('=========== Instantiated Ilab chaincode ===========');
    return shim.success();
  }

  // Invoke Chaincode
  async Invoke(stub) {
    let ret = stub.getFunctionAndParameters();
    console.info(ret);

    let method = this[ret.fcn];
    if (!method) {
      console.error('no function of name:' + ret.fcn + ' found');
      throw new Error('Received unknown function ' + ret.fcn + ' invocation');
    }
    try {
      let payload = await method(stub, ret.params);
      return shim.success(payload);
    } catch (err) {
      console.log(err);
      return shim.error(err);
    }
  }
};
shim.start(new Chaincode());
