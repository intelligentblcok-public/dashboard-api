// ./express-server/app.js
import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import logger from 'morgan';
import cors from 'cors';
import SourceMapSupport from 'source-map-support';
// import routes
import mappingAutoCommisionRoutes from './routes/mapping.server.route';
// define our app using express
const app = express();
// var options = {
//   key: fs.readFileSync('/etc/nginx/cert.key'),
//   cert: fs.readFileSync('/etc/nginx/cert.crt')
// };
// var serverPort = 443;

// var server = https.createServer(options, app);
// var io = require('socket.io')(server);

let http = require("http").Server(app);
let io = require("socket.io")(http);
global.io = io; //added

io.on("connection", socket => {
  // Log whenever a user connects
  console.log("user connected");
  // Log whenever a client disconnects from our websocket server
  socket.on("disconnect", function() {
    console.log("user disconnected");
  });
  socket.on("message", message => {
      console.log("Message Received: " + message);
     // io.emit("dockerLogs", { type: "new-message", log: info.toString() });
    });

  //io.emit("dockerLogs", { type: "new-message", log: info.toString() });

});

// allow-cors
app.use(cors());
// configure app
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended:true }));
app.use(express.static(path.join(__dirname, 'public')));
// set the port
const port = process.env.PORT || 3011;

// add Source Map Support
SourceMapSupport.install();
app.use('/api', mappingAutoCommisionRoutes);
app.get('/', (req,res) => {
  return res.end('Auto-Commision Mapping Api testing... it is working...');
})
// catch 404
app.use((req, res, next) => {
  res.status(404).send('<h2 align=center>Page Not Found!</h2>');
});
// start the server
http.listen(port,() => {
  console.log(`App Server Listening at ${port}`);
});