// Query to fetch channels
var query = require('../app/query.js');
var helper = require('../app/helper.js');
var hfc = require('fabric-client');
var log4js = require('log4js');
var logger = log4js.getLogger('SampleWebApp');
const fs = require('fs');
var logDir = '/var/log/';
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;


var Docker = require('dockerode');
var docker = new Docker({ socketPath: '/var/run/docker.sock' });

export const getNetworkConfig = async function (req, res) {
    var username = req.body.user;
    var orgname = req.body.org;
    let config = '-connection-profile-path';
    let client = await hfc.loadFromConfig(hfc.getConfigSetting('network' + config));
    var channels = client._network_config._network_config.channels;
    var organizations = client._network_config._network_config.organizations;
    var orderers = client._network_config._network_config.orderers;
    var peers = client._network_config._network_config.peers;
    var networkConfigurationObj = {};

    networkConfigurationObj.channels = channels;
    networkConfigurationObj.organizations = organizations;
    networkConfigurationObj.orderers = orderers;
    networkConfigurationObj.peers = peers;

    if (!networkConfigurationObj) {
        return res.json({ 'success': false, 'message': 'Error' })
    } else {
        return res.json({ 'success': true, 'networkResult': networkConfigurationObj });
    }
}

export const getContainerList = async function (req, res) {
    var lst = [];
    // docker.container.list()
    //     // Inspect
    //     .then(containers => {
    //         containers.forEach(function(container){
    //             console.log(container.data.Id);
    //             console.log(container.data.Names);
    //             var obj={id:container.data.Id, Name:container.data.Names[0],Image:container.data.Image };
    //             lst.push(obj);
    //         });

    //         return res.json({ 'success': true, data:lst });

    //     })

    //     .catch(error => {
    //         console.log(error)
    //         return res.json({ 'success': false });
    //     });

    docker.listContainers(function (err, containers) {
        if (err) {
            console.log(err);
            return res.json({ 'success': false });
        }
        containers.forEach(function (container) {
            console.log(container.Id);
            console.log(container.Names);
            var obj = { id: container.Id, Name: container.Names[0], Image: container.Image };
            lst.push(obj);
        });
        return res.json({ 'success': true, data: lst });
    });

}

export const getContainerLogs = async function (req, res) {
    var containerLog = [];
    var from_date = req.body.from_date// new Date(req.body.from_date).getTime() / 1000;
    var to_date = req.body.to_date// new Date(req.body.to_date).getTime() / 1000;
    var container_id = req.body.container_id;
    console.log(container_id);
    var container = docker.getContainer(container_id);

    // container.inspect(function (err, data) {
    //     console.log(data);
    // });

    
    exec('sudo docker logs -t --since '+from_date+' --until '+to_date+' '+container_id, (error, stdout, stderr)=>{
        containerLog.push(stdout);
        console.log(`error: ${error}`);
        console.log(`stdout: ${stdout}`);
        console.log(`stderr: ${stderr}`);
        global.io.emit('dockerLogs', { logs: stderr.toString() });
    });
        // runExe.stdout.on('data', (data) => {
        //     containerLog.push(data);
        //     global.io.emit('dockerLogs', { logs: data.toString() });
        // });

        // runExe.stderr.on('data', (data) => {
        //     console.log(`stderr: ${data}`);
        // });

        // runExe.on('close', (code) => {
        //     console.log(`child process closed with code ${code}`);
        //     return res.json({ 'success': true, containerLog});
        // });

        return res.json({ 'success': true, containerLog});

    // container.logs({
    //     follow: true,
    //     stdout: true,
    //     stderr: true,
    //     since: from_date,
    //     until: to_date,
    //   })
    //   .then(async stream => {
    //     await stream.on('data', info =>{
    //         console.log(info.toString());
    //         containerLog.push(info.toString())
    //         global.io.emit('dockerLogs', { logs: info.toString() });

    //     })
    //     stream.on('error', err => console.log(err))

    //     return res.json({ 'success': true, containerLog });
    //   })
    //   .catch(error =>{
    //     console.log(error);
    //     return res.json({ 'success': false });
    //   });
}

