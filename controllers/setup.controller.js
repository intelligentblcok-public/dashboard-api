//var await = require('await');
require('../config');
var hfc = require('fabric-client');

var helper = require('../app/helper');
var createChannel = require('../app/create-channel');
var join = require('../app/join-channel');
var install = require('../app/install-chaincode');
var instantiate = require('../app/instantiate-chaincode');
var invoke = require('../app/invoke-transaction');
var query = require('../app/query');

export const index = async function(req, res){
await helper.getRegisteredUser('org1Admin', 'Org1', true);
await helper.getRegisteredUser('org2Admin', 'Org2', true);

await createChannel.createChannel('mychannel', '../artifacts/channel/mychannel.tx', 'org1Admin', 'Org1');

await join.joinChannel('mychannel', ['peer0.org1.example.com', 'peer1.org1.example.com'], 'org1Admin', 'Org1');
await join.joinChannel('mychannel', ['peer0.org2.example.com', 'peer1.org2.example.com'], 'org2Admin', 'Org2');


await install.installChaincode(['peer0.org1.example.com', 'peer1.org1.example.com'], 'iBlock', 'iblock', 'v1', 'node', 'org1Admin', 'Org1');
await install.installChaincode(['peer0.org2.example.com', 'peer1.org2.example.com'], 'iBlock', 'iblock', 'v1', 'node', 'org2Admin', 'Org2');

await instantiate.instantiateChaincode(null, 'mychannel', 'iBlock', 'v1', 'node', null, [], 'org1Admin', 'Org1');
console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!completed!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
return res.json({ 'success': true, 'message': 'Chaincode Installed successfully'});

}
